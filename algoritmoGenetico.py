import codecs
import json
import os
import pickle
import random
from datetime import datetime

import pandas as pd
from Neural_Network.Data import Data
from Neural_Network.Model import NN_Model
from Util.ReadFile import get_dataFile

# CONSTANTES DEL ALGORITMO
maximo_generaciones = 10  # Número máximo de generaciones que va a tener el algoritmo
modoSeleccion = '2'
criterio = '1'
file = ""
tamPoblacion = 10
mejorIndividuo = []
mensaje = ""
_modelos = []

_alpha = [0.00001, 0.00005, 0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5]
_lambda = [0, 0.01, 0.05, 0.1, 0.5, 1, 1.5, 2.5, 5.5, 7.5]
_max_iteration = [500, 750, 1250, 1500, 2000, 4000, 7500, 15000, 10000, 5000]
_keep_prob = [1, 0.97, 0.95, 0.9, 0.85, 0.75, 0.65, 0.5, 0.3, 1]
_hiperparametros = [_alpha, _lambda, _max_iteration, _keep_prob]
_train_X, _train_Y, _val_X, _val_Y, _test_X, _test_Y = get_dataFile()


def inicializarPoblacion():
    poblacion = []
    lista = [0.01, 0, 1000, 1]
   # individuo = Nodo(lista, evaluarFitness(lista))
   # poblacion.append(individuo)
    for x in range(tamPoblacion):
        lista = listaAleatorios()
        individuo = Nodo(lista, evaluarFitness(lista))
        poblacion.append(individuo)

    return poblacion  # Retorno la población ya creada


def listaAleatorios():
    lista = [0] * 4

    rand1 = random.randint(0, 9)
    rand2 = random.randint(0, 9)
    rand3 = random.randint(0, 9)
    rand4 = random.randint(0, 9)
    lista[0] = (_alpha[rand1])
    lista[1] = (_lambda[rand2])
    lista[2] = (_max_iteration[rand3])
    lista[3] = (_keep_prob[rand4])

    return lista


def imprimirPoblacion(poblacion):
    for individuo in poblacion:
        # print('Individuo: ', individuo.solucion, ' Valor de x: ', individuo.x, ' Fitness: ', individuo.fitness)
        print('Individuo: ', individuo.solucion, ' Fitness: ', individuo.fitness)


def evaluarFitness(ls):
    fit1, fit2 = entrenarModeloNeuronal(ls[0], ls[1], ls[2], ls[3])

    return fit1


def load_file(file):
    return pd.read_csv(
        file,
        dtype={'PROYECTO 1': 'float64', 'PROYECTO 2': 'float64', 'PROYECTO 3': 'float64', 'PROYECTO 4': 'float64',
               'NOTA FINAL': 'float64'}
    )


def verificarCriterio(poblacion, generacion):
    # Calculo el valor fitness de los individuos
    for individuo in poblacion:
        individuo.fitness = evaluarFitness(individuo.solucion)

    # print("*******criterio1()*******")
    # Mejor fitness de los individuos por numero maximo de generaciones
    if generacion >= maximo_generaciones:
        return True
    else:
        return None


def seleccionarPadres(poblacion):
    # Padres con mejor valor fitness
    # print("*******seleccion2()*******")
    global tamPoblacion
    mitad = round(tamPoblacion / 3)
    padres = sorted(poblacion, key=lambda item: item.fitness, reverse=True)[:mitad]

    return padres


def emparejar(padres):
    global tamPoblacion
    hijos = []
    h1 = Nodo()
    tampadres = len(padres)

    for i in range(tampadres, tamPoblacion):
        index1 = random.randint(0, tampadres - 1)
        index2 = random.randint(0, tampadres - 1)
        while (index1 == index2):
            index2 = random.randint(0, tampadres - 1)

        h1 = Nodo()
        h1.solucion = cruzar(padres[index1].solucion, padres[index2].solucion)
        h1.solucion = mutar(h1.solucion)
        hijos.append(h1)

    for hijo in hijos:
        padres.append(hijo)

    return padres


def cruzar(solucionPadre1, solucionPadre2):
    solucionHijo = []

    ran = random.random()
    if ran <= .5:
        solucionHijo.append(solucionPadre1[0])
    else:
        solucionHijo.append(solucionPadre2[0])

    ran = random.random()
    if ran <= .5:
        solucionHijo.append(solucionPadre1[1])
    else:
        solucionHijo.append(solucionPadre2[1])

    ran = random.random()
    if ran <= .5:
        solucionHijo.append(solucionPadre1[2])
    else:
        solucionHijo.append(solucionPadre2[2])

    ran = random.random()
    if ran <= .5:
        solucionHijo.append(solucionPadre1[3])
    else:
        solucionHijo.append(solucionPadre2[3])

    return solucionHijo


def mutar(solucionHijo):
    ran = random.random()
    if ran <= .5:
        pos = random.randint(0, 3)
        valrand = random.randint(0, 9)
        solucionHijo[pos] = _hiperparametros[pos][valrand]

    return solucionHijo


def ejecutar():
    print("Algoritmo corriendo")

    generacion = 0
    poblacion = inicializarPoblacion()
    fin = verificarCriterio(poblacion, generacion)

    print('*************** GENERACION ', generacion, " ***************")
    imprimirPoblacion(poblacion)

    while (fin == None):
        padres = seleccionarPadres(poblacion)
        poblacion = emparejar(padres)
        generacion += 1  # Lo pongo aquí porque en teoría ya se creó una nueva generación
        fin = verificarCriterio(poblacion, generacion)

        print('*************** GENERACION ', generacion, " ***************")
        imprimirPoblacion(poblacion)

    # Obtengo la mejor solución y la muestro
    arregloMejorIndividuo = sorted(poblacion, key=lambda item: item.fitness, reverse=True)[
                            :1]  # #Ordena de mayor a menor

    global mejorIndividuo
    mejorIndividuo = arregloMejorIndividuo[0]

    print('\n\n*************** MEJOR INDIVIDUO***************')
    print('Individuo: ', mejorIndividuo.solucion, ' Fitness: ', mejorIndividuo.fitness)
    escribirBitacora(generacion, mejorIndividuo.solucion)
    escribirBitacoraNN(_modelos)

    pesos = fin[0][2]
    lw1 = pesos['W1'].tolist()
    lb1 = pesos['b1'].tolist()
    lw2 = pesos['W2'].tolist()
    lb2 = pesos['b2'].tolist()
    lw3 = pesos['W3'].tolist()
    lb3 = pesos['b3'].tolist()
    lw4 = pesos['W4'].tolist()
    lb4 = pesos['b4'].tolist()
    newpesos = {
        'W1':lw1,
        'b1':lb1,
        'W2':lw2,
        'b2':lb2,
        'W3':lw3,
        'b3':lb3,
        'W4':lw4,
        'b4':lb4,
    }
    file_path = "C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Apoyo\\IA1_proyecto2-master\\temporales\\pesos.json" ## your path variable
    json.dump(newpesos, codecs.open(file_path, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)


def escribirBitacora(generaciones, solucion):
    criterioFin = ""
    modoSel = ""

    criterioFin = "Máximo número de generaciones"

    modoSel = "Selección de los padres con mejor valor fitness"

    print("Escribiendo en bitacora")
    f = open("bitacoraAG.txt", "a")
    tiempo = datetime.now()

    contenido = [

        '------------------------------\n',
        'Fecha de ejecución: ' + tiempo.date().isoformat() + '\n',
        'Hora de ejecución: ' + tiempo.time().isoformat() + '\n\n',

        'Criterio de finalización utilizado: ', criterioFin + '\n',
        'Criterio de selección de padres: ', modoSel, '\n',
        'No. Generaciones: ', str(generaciones) + '\n',
        'Mejor solucion: ', str(solucion) + '\n',
        '------------------------------\n',
    ]
    f.writelines(contenido)
    f.close()

    return 0


def entrenarModeloNeuronal(alpha=0.001, lambd=0, iterations=50000, keep_prob=0.5):
    # Cargando conjunto de datos

    # Definir los conjuntos de datos
    train_set = Data(_train_X, _train_Y)
    val_set = Data(_val_X, _val_Y)
    test_set = Data(_test_X, _test_Y)

    # Se define las dimensiones de las capas
    # la capa de entrada NO se toma en cuenta
    capas1 = [train_set.n, 10, 8, 6, 4, 1]

    # Se define el modelo
    nn1 = NN_Model(train_set, capas1, alpha=alpha, iterations=iterations, lambd=lambd, keep_prob=keep_prob)

    # Se entrena el modelo

    print('Entrenamiento Modelo: alpha' + str(alpha) + ', Lamb:' + str(lambd) + ', It:' + str(
        iterations) + ', keep_prob:' + str(keep_prob) + ',')
    nn1.training(False)
    print('Validacion Modelo')
    exactitudValidacion = nn1.predict(val_set)
    print('Pruebas Modelo')
    exactitudPruebas = nn1.predict(test_set)
    _modelos.append(nn1)

    return exactitudValidacion, exactitudPruebas


def escribirBitacoraNN(modelos):
    print("Escribiendo en bitacora")
    tiempo = datetime.now()
    modelos = sorted(modelos, key=lambda item: item.test_accuracy, reverse=True)

    # serializo el mejor modelo  y lo guardo
    with open('data.pickle', 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump(modelos[0], f, pickle.HIGHEST_PROTOCOL)

    f = open("bitacoraNN.txt", "a")
    fecha = [

        'Fecha de ejecución: ' + tiempo.date().isoformat() + '\n',
        'Hora de ejecución: ' + tiempo.time().isoformat() + '\n',
    ]
    f.writelines(fecha)

    for model in modelos:
        contenido = [
            '------------------------------\n',
            'exactitud-validacion: ', str(model.test_accuracy) + '\n',
            'layers: ', str(model.layers) + '\n',
            'alpha: ', str(model.alpha) + '\n',
            'lambd: ', str(model.lambd) + '\n',
            'iterations: ', str(model.max_iteration) + '\n',
            'keep_prob: ', str(model.kp) + '\n',
            '------------------------------\n',
        ]
        f.writelines(contenido)

    f.close()

    return 0


def cargarModelo():
    with open('data.pickle', 'rb') as f:
        # The protocol version used is detected automatically, so we do not
        # have to specify it.
        data = pickle.load(f)
        return data


class Nodo:
    # solucion = []
    # fitness = 0 #Valor fitness
    # x = 0 #Para la tarea se guarda el valor de x

    # Le defino parámetros al constructor y le pongo valores por defecto por si no se envían
    def __init__(self, poblacion=[], fitness=0, x=0):
        self.solucion = poblacion
        self.fitness = fitness
        self.x = x


# cargarModelo()
ejecutar()
