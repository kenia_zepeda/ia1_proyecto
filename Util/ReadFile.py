import scipy.io
import pandas as pd
from math import radians, cos, sin, asin, sqrt
import codecs, json

'''def get_dataFile():
    data = scipy.io.loadmat('datasets/data.mat')
    
    train_X = data['X'].T
    train_Y = data['y'].T
    val_X = data['Xval'].T
    val_Y = data['yval'].T

    return train_X, train_Y, val_X, val_Y'''


def get_dataFile():
    #Cargo archivo con Datos ya tratados
    df = pd.read_csv(
        'C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Proyecto\\IA1_Proyecto\\datasets\\Data.csv',
        dtype={'Distancia': 'float64', 'Estado': 'int64', 'Genero': 'int64', 'Edad': 'float64', 'Anio': 'float64'}
    )
    Y = df['Estado']
    X = df[['Distancia', 'Genero', 'Edad', 'Anio']]
    train_X = X[0:6000].to_numpy()
    train_Y = Y[0:6000].to_numpy()
    val_X = X[6001:6820].to_numpy()
    val_Y = Y[6001:6820].to_numpy()
    test_X = X[6821:7584].to_numpy()
    test_Y = Y[6821:7584].to_numpy()

    return train_X.T, train_Y.T, val_X.T, val_Y.T, test_X.T, test_Y.T

def get_dataFileDataset():
    #Cargo archivo de datos iniciales
    return pd.read_csv(
        'C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Proyecto\\IA1_Proyecto\\datasets\\Dataset.csv',
        dtype={'Estado': 'object', 'Genero': 'object', 'Edad': 'int64', 'Anio': 'int64', 'cod_depto': 'int64',
               'cod_muni': 'int64', 'nombre': 'object', 'municipio': 'object'}
    )

def get_dataFileMunicipios():
    #cargo archivo con informacion de la distancias en municio y universidad
    return pd.read_csv(
        'C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Proyecto\\IA1_Proyecto\\datasets\\Municipios.csv',
        dtype= {'Depto':'int64','Muni':'int64','lat':'float64','lon':'float64'}
        )


def tratamiento_de_datos():
    # cargo Dataset.csv
    datos1 = get_dataFileDataset()
    # cargo Municipios.csv
    datos2 = get_dataFileMunicipios()
    # ordeno la columna de cod_depto y Depto
    datos1.sort_values(by=['cod_depto'], inplace=True)
    datos2.sort_values(by=['Depto'], inplace=True)
    distancia = []
    latU = 14.589246
    lonU = -90.551449
    #Comparo datos de los dos archivos de entra para calcular la distancia
    for index, row in datos1.iterrows():
        for index2, row2 in datos2.iterrows():
            if row['cod_depto'] == row2['Depto'] and row['cod_muni'] == row2['Muni']:
                lat = row2['Lat']
                lon = row2['Lon']
                #calculo la distancia con la formula haversine
                distancia.append(haversine(latU, lonU, lat, lon))
                break

    #Creo la columna con los datos de las distancias calculadas
    datos1['Distancia2'] = distancia


    #obtengo los maximos y minimos de Edad, Anio y Distancia para el escalamiento
    minedad = datos1['Edad'].min()
    maxedad = datos1['Edad'].max()
    factoredad = maxedad - minedad
    minanio = datos1['Anio'].min()
    maxanio = datos1['Anio'].max()
    factoranio = maxanio - minanio
    mindist = datos1['Distancia2'].min()
    maxdist = datos1['Distancia2'].max()
    factordist = maxdist - mindist

    #guardo maximos y minimos en un json para su futuro uso
    dict = {
        'minedad': minedad.item(),
        'maxedad': maxedad.item(),
        'minanio': minanio.item(),
        'maxanio': maxanio.item(),
        'mindist': mindist.item(),
        'maxdist': maxdist.item()
    }
    file_path = "C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Proyecto\\IA1_Proyecto\\datasets\\escalamiento.json"  ## your path variable
    json.dump(dict, codecs.open(file_path, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True,
              indent=4)

    listdist = []
    listedad = []
    listanio = []
    #Hago calculos para el escalamiento de datos y los voy guardando en listas
    for index, row in datos1.iterrows():
        listdist.append((row['Distancia2'] - mindist) / factordist)
        listedad.append((row['Edad'] - minedad) / factoredad)
        listanio.append((row['Anio'] - minanio) / factoranio)
    #Creo el dataframe con todos lo datos ya tratados para guardarlos en un nuevo Csv
    df = pd.DataFrame(list(zip(listdist, listedad, listanio)), columns=['Distancia', 'Edad', 'Anio'])

    # Asigno 0 si hombre o 1 si es mujer
    df['Genero'] = datos1['Genero'].apply(lambda x: 0 if x == 'MASCULINO' else 1)

    # Asigno 0 al estado si es Traslado o 1 si es Activo
    df['Estado'] = datos1['Estado'].apply(lambda x: 0 if x == 'Traslado' else 1)

    #Guardo datos en un nuevo csv
    df.to_csv('C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Proyecto\\IA1_Proyecto\\datasets\\Data.csv')


def get_dataForPredict(genero, edad, anio, dep, mun):
    print("----------Creando datos de entrada--------------------")
    #tratamiento de datos, abrir maximos y minimos
    obj_text = codecs.open('C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Proyecto\\IA1_Proyecto\\datasets\\escalamiento.json', 'r', encoding='utf-8').read()
    datos = json.loads(obj_text)
    #calcular la distancia
    distancia = 0
    latU = 14.589246
    lonU = -90.551449
    datos2 = get_dataFileMunicipios()
    for index2, row2 in datos2.iterrows():
        if dep == row2['Depto'] and mun == row2['Muni']:
            lat = row2['Lat']
            lon = row2['Lon']
            #calcula la distancia con la formula de haversine
            distancia = haversine(latU, lonU, lat, lon)
            break

    #escalamiento de distancia
    realdist = (distancia - datos['mindist']) / (datos['maxdist'] - datos['mindist'])
    #escalamiento de anio
    realanio = (anio - datos['minanio']) / (datos['maxanio'] - datos['minanio'])
    #escalamiento de edad
    realedad = (edad - datos['minedad']) / (datos['maxedad'] - datos['minedad'])
    #creo el dataframe con los datos para predecir
    data = list(zip([0], [realdist], [genero], [realedad], [realanio]))
    cols = ['Estado', 'Distancia', 'Genero', 'Edad', 'Anio']
    df = pd.DataFrame(data, columns=cols)
    #dataset en Y
    Y = df['Estado']
    #dataset en X
    X = df[['Distancia', 'Genero', 'Edad', 'Anio']]
    train_X = X.to_numpy()
    train_Y = Y.to_numpy()
    return train_X.T, train_Y.T

def haversine(lon1, lat1, lon2, lat2):
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371 #Radio de la tierra, usa 3956 millas
    return c * r

#tratamiento_de_datos()