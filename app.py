import pickle

from flask import Flask, render_template, request
from Util.ReadFile import get_dataForPredict, get_dataFile
from Util import Plotter
import pandas as pd
from Neural_Network.Data import Data
from Neural_Network.Model import NN_Model
import numpy as np
import codecs, json

app = Flask(__name__)

_train_X, _train_Y, _val_X, _val_Y, _test_X, _test_Y = get_dataFile()


def cargarModelo():
    with open('data.pickle', 'rb') as f:
        # The protocol version used is detected automatically, so we do not
        # have to specify it.
        data = pickle.load(f)
        return data

@app.route('/')
def index():
    # Se analiza el entrenamiento
    nn1 = cargarModelo()
    Plotter.show_Model([nn1])
    return render_template("index.html", mensaje=('Holiii', '', ''))


@app.route('/analizar', methods=['POST', 'GET'])
def analizar():
    resultado = None
    mensaje = ""
    if request.method == 'POST':
        gen = request.form['genero']
        edad = request.form['edad']
        anno = request.form['anno']
        depto = request.form['depto']
        muni = request.form['mun']
        print(gen, edad, anno, depto, muni, sep=',')
        # ejecutar analisis
        resultado, exactitud, yhat = predecir(int(gen), int(edad), int(anno), depto, muni)
        print(resultado)
        mensaje += "\n-----------------------------------\nResultado: " + resultado + "\nYhat: " + str(yhat)

    return render_template("index.html", mensaje=(mensaje, "", ""))


def predecir(genero, edad, anio, dep, mun):  # return y_hat?

    _nn1 = cargarModelo()

    train_X, train_Y = get_dataForPredict(genero, edad, anio, dep, mun)
    test_set = Data(train_X, train_Y)

    # print('Entrenamiento Modelo 1')
    # nn1.predict(train_set)
    # print('Validacion Modelo 1')
    # nn1.predict(val_set)
    print('-----PREDICCION-----')
    exactitud, y_hat = _nn1.predict0(test_set)
    print(exactitud)
    print(y_hat)
    if y_hat[0][0] >= 0.5:
        return "Traslado",exactitud,y_hat[0][0]
    else:
        return "Activo",exactitud,y_hat[0][0]


if __name__ == '__main__':
    app.run()
