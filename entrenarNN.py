import pickle
import numpy as np
import codecs, json
from Util.ReadFile import get_dataFile
from Util import Plotter
from Neural_Network.Data import Data
from Neural_Network.Model import NN_Model

ONLY_SHOW = False

# Cargando conjunto de datos

_train_X, _train_Y, _val_X, _val_Y, _test_X, _test_Y = get_dataFile()
def entrenarNN():
    # Definir los conjuntos de datos
    train_set = Data(_train_X, _train_Y)
    val_set = Data(_val_X, _val_Y)
    test_set = Data(_test_X, _test_Y)

    # Se define las dimensiones de las capas
    # la capa de entrada NO se toma en cuenta
    capas1 = [train_set.n, 10,8,6,4, 1]

    # Se define el modelo
    #nn1 = NN_Model(train_set, capas1, alpha=0.001, iterations=50000, lambd=0, keep_prob=0.5)
    nn1 = NN_Model(train_set, capas1, alpha=0.01, iterations=100000, lambd=0.1, keep_prob=1)


    # Se entrena el modelo
    nn1.training(False)

    print('Entrenamiento Modelo 1')
    nn1.predict(test_set)
    print('Validacion Modelo 1')
    nn1.predict(val_set)

    with open('data.pickle', 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump(nn1, f, pickle.HIGHEST_PROTOCOL)







#entrenarNN()
